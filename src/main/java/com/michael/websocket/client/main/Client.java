package com.michael.websocket.client.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Client {

	public static Map<String, String> messageMap = null;
	public static int threadCount = 0;
	public static Logger logger = Logger.getLogger("MyLog");
	public static FileHandler fh;
	public static SimpleFormatter formatter = new SimpleFormatter();  

	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException{

        // This block configure the logger with handler and formatter  
        fh = new FileHandler("/Users/michael/Development/personal/learnJavaSocket/client.log");  
        logger.addHandler(fh);
        fh.setFormatter(formatter);  

        Client.messageMap = new HashMap<String, String>();
		Client.messageMap.put("1", "{\"event\": \"PLAY\",\"message\": \"This is test message for PLAY\"}");
		Client.messageMap.put("2", "{\"event\": \"PAUSE\",\"message\": \"This is test message for PAUSE\"}");
		Client.messageMap.put("3", "{\"event\": \"SEEK\",\"message\": \"This is test message for SEEK\"}");
		Client.messageMap.put("4", "{\"event\": \"BITRATE\",\"message\": \"This is test message for BITRATE\"}");
		

		int numberOfThreads = 1;

		while(true) {
			
			if( numberOfThreads < 20000) {

				new ClientThread().start();
				numberOfThreads++;				

			}
			else {
				Thread.sleep(1000);
			}
		}
	}
}

class ClientThread extends Thread {

	Socket sock = null;
	InputStream in = null;
	OutputStream out = null;
	int connectionNumber = 0;

	public ClientThread() throws IOException {

		sock = new Socket("127.0.0.1", 9999);
		in = sock.getInputStream();
		out = sock.getOutputStream();

		connectionNumber = Client.threadCount++;
		System.out.println("Client thread initialized - " + connectionNumber);

		Client.logger.info("Client thread initialized - " + connectionNumber); 
	}

	public void run() {

		int count = 1;
		try {
			while(true) {

				String message = (String)Client.messageMap.get(String.valueOf(count));
				out.write(message.getBytes());

				Thread.sleep(10);

				String logMessage = "Message sent for Connection [" + connectionNumber + "] is {" + message + "}";
				Client.logger.info(logMessage); 
				System.out.println(logMessage);

				count++;
				if( count > 4)
					count=1;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
